
################################################################################
# Platform Configuration
################################################################################

menu "Platform Setup"

config BBQUE_TEST_PLATFORM_DATA
  bool "Use Test Platform Data (TPD)"
  default y if TARGET_ANDROID
  default n
  ---help---
  Build the BarbequeRTRM with support for Test Platform Data.
  When compiled with this option turned on, all the target specific
  platform control facilities are disabled.

  This could be a convenient way to use the BarbequeRTRM for initial
  application development and integration, without worry about daemon
  setup or requiring to run the daemon as root.


menu "Linux Control Groups"
depends on !BBQUE_TEST_PLATFORM_DATA

config BBQUE_LINUX_CG_MEMORY
  bool "Memory controller"
  default y
  depends on TARGET_LINUX
  depends on !BBQUE_TEST_PLATFORM_DATA
#  depends on !TARGET_ODROID_XU
  ---help---
  Enable the Control Groups memory controller for enforcing the memory
  assignment control.

endmenu # Linux Control Groups


choice
depends on !BBQUE_TEST_PLATFORM_DATA
  prompt "Memory locality"
  default BBQUE_MEMLOC_L3

  config BBQUE_MEMLOC_L3
    bool "L3 cache"

  config BBQUE_MEMLOC_L2
    bool "L2 cache"

  depends on !BBQUE_TEST_PLATFORM_DATA
  ---help---
  Select the memory locality that the BarbequeRTRM optimization policy should
  take into account to properly perform scheduling and resource allocation
  decisions.
  According to the option selected the resource manager can consider a CPU as
  an entity featuring one or more cores (processing elements), sharing a L3
  cache memory ("socket") or a L2 cache memory ("core").

endchoice # Memory locality


menu "OpenCL Support"
depends on !BBQUE_TEST_PLATFORM_DATA

config BBQUE_OPENCL
  bool "OpenCL Run-time Management Support"
  depends on TARGET_LINUX
  depends on !BBQUE_TEST_PLATFORM_DATA
  select EXTERNAL_OPENCL
  default n
  ---help---
  Enable the support for run-time resource management of OpenCL applications.

endmenu # OpenCL Support

menu "Power Management Support"
depends on !BBQUE_TEST_PLATFORM_DATA
	source "barbeque/bbque/pm/Kconfig"

endmenu # Power Management

endmenu # PIL Configuration

################################################################################
# Policies
################################################################################

menu "Optimization Policy"
  source "barbeque/plugins/schedpol/Kconfig"

comment "Advanced Options"

config BBQUE_SP_PARALLEL
  bool "Policy Parallel Execution"
  default y
  ---help---
  If selected enable the multi-threaded/parallel execution of the scheduling policy.
  This in case of policy implementation supporting sequential and parallel
  execution flow. It can be useful to disable this option for debug purpose.

  Select this option if you plan to run the BarbequeRTRM daemon on a multi-core
  host machine.

  If unsure, say Y

config BBQUE_SP_COWS_BINDING
  bool "Co-scheduling Workloads (COWS) binding"
  depends on !BBQUE_OPENCL
  default n
  ---help---
  Enable this option only if the recipes feature information about some
  performance counters based statistics. and the scheduling policy implements
  the support to exploit such information. This potentially leads to a more
  effective binding (i.e. selection of the resource domain of the AWM) in order
  to establish whether it is better to co-schedule applications on the same
  resource binding domain (e.g., CPU node) or spread them among different
  domains.

  If unsure, say N

endmenu

################################################################################
# Recipe loader
################################################################################

menu "Recipe Loader"
  source "barbeque/plugins/rloader/Kconfig"
endmenu


################################################################################
# RTLib
################################################################################

menu "Run-Time Library (RTLib)"

choice
  prompt "RPC Channel"

  config BBQUE_RPC_FIFO
    bool "FIFO based"
    ---help---
    Use the FIFO based RPC channel

endchoice

config BBQUE_RPC_TIMEOUT
  int "RPC Channel Timeout"
  default 5000
  ---help---
  The amount of time (in seconds) after which a not responding application
  must be considered disabled.

config BBQUE_RTLIB_PERF_SUPPORT
  bool "Performance Counters Profiling Support"
  depends on TARGET_LINUX
  default y
  ---help---
  Build the Run-Time Library (RTLib) with support for Performance Counters.
  These counters allows an application, which has been integrated with the
  RTLib, to collect transparently a set of profiling information related
  to the run-time execution.

  This option is useful to port easily the RTLib on platforms not supporting
  performance counters, such as some Android devices with older kernels.

config BBQUE_RTLIB_CGROUPS_SUPPORT
  bool "CGroups Profiling Support"
  depends on TARGET_LINUX
  depends on !BBQUE_TEST_PLATFORM_DATA
  default n if BBQUE_TEST_PLATFORM_DATA
  default y
  ---help---
  Build the Run-Time Library (RTLib) with support for CGroups.
  This framework allows an application, which has been integrated with the
  RTLib, to collect transparently a set of profiling information related
  to the run-time execution.

  This option is useful to port easily the RTLib on platforms not supporting
  or using CGroups for run-time resources management, such as some Android
  devices.

config BBQUE_RTLIB_UNMANAGED_SUPPORT
  bool "Unmanaged Applications Support"
  depends on TARGET_LINUX
  default y
  ---help---
  Build the Run-Time Library (RTLib) with support for UNMANAGED applications.

  An application running in UNMANAGED mode is not controlled by the BarbequRTRM and
  its automatically configured by the RTLib to run in AWM 0 (by default).
  Once this support is enabled, an application could be executed un UNMANAGED mode
  by setting the "U" flag in the BBQUE_RTLIB_OPTS environment variable.
  Such flag could be followed by the ID of the AWM the application should be configured
  to run into, for example:
    BBQUE_RTLIB_OPTS="U3"
  will run the application in AWM 3.

  This option is useful to run applications without BarbequeRTRM control, for example to
  easily support Design Space Exploration (DSE) or the initial development stages of
  a new application.

  Say NO to this option if unsure.

menu "Performance API Configuration"

config BBQUE_RTLIB_GGAP_THRESHOLD_FORWARD
  int "GoalGap Forward Threshold"
  default 20
  ---help---
  The minimum GoalGap value which is forwarded to BarbequeRTRM.

  Once an EXC asserts a GoalGap, the RTLib can decide to do not forward this
  request if it is not greater than a predefined and configured value.
  This configuration defines the default for that threshold value.

config BBQUE_RTLIB_GGAP_RATE_FORWARD
  int "GoalGap Forward Rate"
  default 1
  ---help---
  A periodical number of execution cycles, after which a GoalGap value can
  forwarded to BarbequeRTRM.

  Once an EXC explicitly asserts a GoalGap, or set a performance goal
  (SetCPSGoal) the RTLib can periodically fire a goal gap assertion only after
  the specified number of cycles.

config BBQUE_RTLIB_CPS_TIME_SAMPLES
  int "Cycle-time Samples for Mean computation"
  default 3
  ---help---
  The number of samples that must be cumulated in the exponential mean
  computation of the RTLIB "cycle-time".

  A value equal to 1 means to get the instantaneous value of time sampled
  during the previous execution cycle.  The higher is the value specified, the
  slower is the variation of mean cycle time.

endmenu # Performance



comment "Advanced Options"

config BBQUE_YM_SYNC_FORCE
  bool "Synchronization Point forcing"
  default n
  ---help---
  Enable the Sync Point forcing via Synchronization Manager and RTLib.

  By selecting this option, the Synchronization Manager (YM) enforce EXCs
  reconfiguration to be aligned with the synchronization latency defined by
  applications being reconfigured.
  The synchronization latency is the time delay required by an application to
  be in a synchronization point, i.e. complete the current run and being ready
  to start a new one.

  In general this option could be keept disabled, since it is mandatory just
  on few specific platforms and programming paradigms combinations.
  Specifically, it should be explicitly enabled just when a resource
  contention treat could be expected on a specific target platform or
  programming paradigm.

  NOTE: disabling Sync Point Enforcing is corrently not completely safe and
        could lead to a reconfiguration miss for some applications.
        Use this feature at your own risk.

  If unsure, select N

config BBQUE_YP_SASB_ASYNC
  bool "Parallel Re-configurations"
  default n if TARGET_ANDROID
  default y
  ---help---
  Build the SASM policy with support for asynchronous protocol calls.

  By selecting this option, the synchronization protocol is executed with
  asynchronous calls which means that the interaction with applications to be
  re-configured is parallelized, i.e. each protocol requets is managed by a
  dedicated thread and all them runs in parallel.

  Select this option if you plan to run the BarbequeRTRM daemon on a multi-core
  host machine and with a average high number of concurrent applications.

  If unsure, select Y. Select N for Android targets.

endmenu # RTLib Configuration

################################################################################
# Advanced Options
################################################################################

menu "Advanced Options"

config BBQUE_BUILD_DEBUG
  bool "Build DEBUG version"
  default n
  ---help---
  Build the BarbequeRTRM with DEBUGGING support enabled.

  This will produce a version of both the daemon and the RTLib with much more
  logging messages and debug symbols, which is suitable for debugging purposes.

config BBQUE_GGAP_THRESHOLD_OPTIMIZE
  int "GoalGap Optimize Threshold"
  default 40
  ---help---
  The minimum GoalGap value which triggers an optimization.

  Once an EXC asserts a GoalGap, an optimization event is generated within
  the BarbequeRTRM only if the asserted value is greater than a predefined
  and configured value.
  This configuration defines the default for that threshold value.

config BBQUE_APP_PRIO_LEVELS
  int "Number of Priority Levels"
  default 5
  ---help---
  Applications are scheduled according to their priority which is a value
  ranging from 0 (highest priority) to a defined minimum value.

  Recipies define the priority of the corresponding Application.

config BBQUE_RESOURCE_MAX_NUM
  int "Number of Resources per Type"
  default 16
  ---help---
  The maximum number of resources per resource type.

  For example, the maximum number of processing elements (computing cores)
  available per processor.


config BBQUE_SCHED_PROFILING
  bool "Scheduling Profiling"
  default n
  ---help---
  Enable the scheduling profiling support

  This will collect statistics about the resource allocation metrics (e.g.,
  mean and variance of the AWM from the scheduled applications, fairness
  index, etc...)

config BBQUE_BUILD_TESTS
  bool "Build the TEST suite"
  default n
  ---help---
  Build the BarbequeRTRM test suite.

  This will build also the set of regression tests used to verify framework
  stability during development.
  NOTE: this is an experimental and not completed feaure, leave un-selected is
  unsure.

endmenu # Advanced options
