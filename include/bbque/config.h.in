/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BBQUE_CONFIG_H_
#define BBQUE_CONFIG_H_

/*******************************************************************************
 *                *** This file has been auto-generated ***
 *
 * To update the BarbequeRTRM building configuration, please run the
 * configuration menu using the command:
 *   make menuconfig
 * from the BOSP installation directory
 ******************************************************************************/

#include <syslog.h>
#define  __STDC_FORMAT_MACROS
#include <cinttypes>
#include <unistd.h>

/** Use Boost Filesyste library version 3 */
#define BOOST_FILESYSTEM_VERSION 3

/*******************************************************************************
 * Installation PATHs
 ******************************************************************************/

/** The Barbeque DAEMON name */
#define BBQUE_DAEMON_NAME "${BBQUE_DAEMON_NAME}"

/** The Barbeque DAEMON user ID */
#define BBQUE_DAEMON_UID "${BBQUE_DAEMON_UID}"

/** The Barbeque DAEMON 'lockfile' path */
#define BBQUE_DAEMON_LOCKFILE "${BBQUE_DAEMON_LOCKFILE}"

/** The Barbeque DAEMON 'pidfile' path */
#define BBQUE_DAEMON_PIDFILE "${BBQUE_DAEMON_PIDFILE}"

/** The Barbeque DAEMON run-directory path */
#define BBQUE_DAEMON_RUNDIR "${BBQUE_DAEMON_RUNDIR}"

/** The installation path */
#define BBQUE_PATH_PREFIX "${CONFIG_BOSP_RUNTIME_PATH}"

/** The 'barbeque' installation path */
#define BBQUE_PATH_BBQ "${BBQUE_PATH_BBQ}"

/** The 'conf' installation path */
#define BBQUE_PATH_CONF "${BBQUE_PATH_CONF}"

/** The 'temporary' directory path */
#define BBQUE_PATH_TEMP "${BBQUE_PATH_TEMP}"

/** The 'recipes' installation path */
#define BBQUE_PATH_RECIPES "${BBQUE_PATH_RECIPES}"

/** The 'headers' installation path */
#define BBQUE_PATH_HEADERS "${BBQUE_PATH_HEADERS}"

/** The 'rtlib' installation path */
#define BBQUE_PATH_RTLIB "${BBQUE_PATH_RTLIB}"

/** The 'plugins' installation path */
#define BBQUE_PATH_PLUGINS "${BBQUE_PATH_PLUGINS}"

/** The 'docs' installation path */
#define BBQUE_PATH_DOCS "${BBQUE_PATH_DOCS}"

/** The 'var' installation path */
#define BBQUE_PATH_VAR "${CONFIG_BOSP_RUNTIME_RWPATH}"

/** The RPC channel TIMEOUT */
#define BBQUE_RPC_TIMEOUT ${BBQUE_RPC_TIMEOUT}

/** The Sync protocol TIMEOUT */
#define BBQUE_SYNCP_TIMEOUT ${BBQUE_RPC_TIMEOUT}

/** The Barbeque configuration file */
#define BBQUE_CONF_FILE "${BBQUE_CONF_FILE}"

/** The full path and filename of the BarbequeRTRM configuration file */
#define BBQUE_CONF_FILEPATH BBQUE_PATH_PREFIX "/" BBQUE_PATH_CONF "/" BBQUE_CONF_FILE

/** The RTLib configuration file */
#define BBQUE_RTLIB_CONF_FILE "${BBQUE_RTLIB_CONF_FILE}"

/** The full path and filename of the RTLib configuration file */
#define BBQUE_RTLIB_CONF_FILEPATH BBQUE_PATH_PREFIX "/" BBQUE_PATH_CONF "/" BBQUE_RTLIB_CONF_FILE


/*******************************************************************************
 * BarbequeRTRM Supported Features
 ******************************************************************************/

/** Target platform: Generic-Linux */
#cmakedefine CONFIG_TARGET_LINUX

/** Target platform: Android-Linux */
#cmakedefine CONFIG_TARGET_ANDROID

/** Target platform: ARM Architecture */
#cmakedefine CONFIG_TARGET_ARM

/** Target hardware */
#cmakedefine CONFIG_TARGET_ARM_BIG_LITTLE
#cmakedefine CONFIG_TARGET_ARM_CORTEX_A9
#cmakedefine CONFIG_TARGET_ARM_CORTEX_A7
#cmakedefine CONFIG_TARGET_ARM_CORTEX_A15
#cmakedefine CONFIG_TARGET_ODROID_XU
#cmakedefine CONFIG_TARGET_ARNDALE_OCTA
#cmakedefine CONFIG_TARGET_PANDABOARD_ES
#cmakedefine CONFIG_TARGET_ANDROID_EMULATOR
#cmakedefine CONFIG_TARGET_FREESCALE_IMX6Q

/** Big.LITTLE: high-performance cores */
#define BBQUE_BIG_LITTLE_HP "${CONFIG_TARGET_ARM_BIG_LITTLE_HP}"

/** Target platform hardware id */
#define BBQUE_TARGET_HARDWARE_ID "${CONFIG_TARGET_ID}"

/** Number of CPU cores */
#define BBQUE_TARGET_CPU_CORES_NUM ${CONFIG_TARGET_CPU_CORES_NUM}

/** Use FIFO based RPC channel */
#cmakedefine CONFIG_BBQUE_RPC_FIFO

/** Use Test Platform Data */
#cmakedefine CONFIG_BBQUE_TEST_PLATFORM_DATA

/* Enable Linux Control Groups 'memory' controller */
#cmakedefine CONFIG_BBQUE_LINUX_CG_MEMORY

/** Memory locality: L3 cache */
#cmakedefine CONFIG_BBQUE_MEMLOC_L3

/** Memory locality: L2 cache */
#cmakedefine CONFIG_BBQUE_MEMLOC_L2

/** Enable OpenCL applications support */
#cmakedefine CONFIG_BBQUE_OPENCL

/** Selected OpenCL platform */
#define BBQUE_OPENCL_PLATFORM "${OPENCL_PLATFORM}"

/** OpenCL vendor library */
#define BBQUE_OPENCL_PATH_LIB "${OPENCL_LIBRARY}"


/** Enable Power Management support */
#cmakedefine CONFIG_BBQUE_PM

/** Enable periodic Power monitoring */
#cmakedefine CONFIG_BBQUE_WM

/** Enable Power Management support */
#cmakedefine CONFIG_BBQUE_PM_BATTERY

/** Enable AMD Power Management support */
#cmakedefine CONFIG_BBQUE_PM_AMD

/** Enable CPU Power Management support */
#cmakedefine CONFIG_BBQUE_PM_CPU

/** Enable GPU Power Management support for ARM Mali */
#cmakedefine CONFIG_BBQUE_PM_GPU_ARM_MALI

/** Performance Counters Support */
#cmakedefine CONFIG_BBQUE_RTLIB_PERF_SUPPORT

/** Unmanaged Mode Support */
#cmakedefine CONFIG_BBQUE_RTLIB_UNMANAGED_SUPPORT

/** CGroups Support */
#cmakedefine CONFIG_BBQUE_RTLIB_CGROUPS_SUPPORT

/** Log4CPP Support */
#cmakedefine CONFIG_EXTERNAL_LOG4CPP

/** Enabled YaMS Scheduling policy parallel execution */
#cmakedefine CONFIG_BBQUE_SP_PARALLEL

/** Enabled YaMS Scheduling policy parallel execution */
#cmakedefine CONFIG_BBQUE_SP_COWS_BINDING

/** Enabled Synchronization Manager sync point enforcing */
#cmakedefine CONFIG_BBQUE_YM_SYNC_FORCE

/** Enabled SASB Synchronization policy asynchronous calls */
#cmakedefine CONFIG_BBQUE_YP_SASB_ASYNC

/** Enable the scheduling policy profiling  */
#cmakedefine CONFIG_BBQUE_SCHED_PROFILING

/** Target platform support C++11 required features */
#cmakedefine CONFIG_TARGET_SUPPORT_CPP11

/** Target architecture */
#define BBQUE_TARGET_ARCH_${BBQUE_TARGET_ARCH}BIT

/** Build flavor */
#define BBQUE_BUILD_FLAVOR "${BBQUE_BUILD_FLAVOR}"

/*******************************************************************************
 * BarbequeRTRM Plugins Selection
 ******************************************************************************/

#define BBQUE_SCHEDPOL_DEFAULT "${BBQUE_SCHEDPOL_DEFAULT}"

#define BBQUE_RLOADER_DEFAULT "${BBQUE_RLOADER_DEFAULT}"

/*******************************************************************************
 * BarbequeRTRM Global Features
 ******************************************************************************/

/**
 * @brief The (default) minimum priority for an Application
 *
 * Applications are scheduled according to their priority which is a
 * value ranging from 0 (highest priority) to a Bbque defined minimum values.
 * The default minimum value is defined by BBQUE_APP_PRIO_LEVELS, but it can be
 * tuned by a Barbeque configuration parameter.
 *
 * Recipies could define the priority of the corresponding Application.
 */
#define BBQUE_APP_PRIO_LEVELS ${CONFIG_BBQUE_APP_PRIO_LEVELS}

/**
 * @brief The maximum number of resources per resource type
 *
 * For example, the maximum ID number of a processing element (computing
 * cores) of a processor. This value is used for initializing the resource
 * bitset type.
 */
#define BBQUE_MAX_R_ID_NUM ${CONFIG_BBQUE_RESOURCE_MAX_NUM}-1

/**
 * @brief The type used to represent UIDs
 *
 * The UID is a unique identifier for each BabrequeRTRM managed application.
 * This defines the type used to represent this identifiers.
 */
#define BBQUE_UID_TYPE uint32_t

/**
 * @brief The GoalGap Forward Threshold
 *
 * The RTLib do not forward GoalGap requests which are, by default, lower than
 * that value. The run-time value could be configured via the 't' flag of the
 * BBQUE_RTLIB_OPTS environment variable.
 */
#define BBQUE_DEFAULT_GGAP_THRESHOLD_FORWARD ${CONFIG_BBQUE_RTLIB_GGAP_THRESHOLD_FORWARD}

/**
 * @brief The GoalGap Optimize Threshold
 *
 * An optimization event is generated within the BarbequeRTRM only if the
 * asserted GoalGap value is greater than a predefined and configured value.
 * The run-time value could be configured via the:
 *   ApplicationManager.ggap.threshold_optimize
 * parameter of the BarbequeRTRM configuration file.
 */
#define BBQUE_DEFAULT_GGAP_THRESHOLD_OPTIMIZE ${CONFIG_BBQUE_GGAP_THRESHOLD_OPTIMIZE}

/**
 * @brief The GoalGap Forward Rate
 *
 * The RTLib can be configured to forward a goal-gap only every N cycles.
 * Where N is the value of this paramenter.
 */
#define BBQUE_DEFAULT_GGAP_RATE_FORWARD ${CONFIG_BBQUE_RTLIB_GGAP_RATE_FORWARD}

/**
 * @brief CPS: Cycle-time number of samples for EMA computation
 *
 * The number of samples that must be cumulated in the exponential mean
 * computation of the RTLIB "cycle-time".
 * A value equal to 1 means to get the instantaneous value of time sampled
 * during the previous execution cycle.  The higher is the value specified,
 * the slower is the variation of mean cycle time.
 */
#define BBQUE_RTLIB_CPS_TIME_SAMPLES ${CONFIG_BBQUE_RTLIB_CPS_TIME_SAMPLES}

/**
 * @brief The number of bits used to represent the EXC_ID into the UID
 *
 * Internally to the BarbequeRTRM, each application is represented by a UID
 * which is obtained using this formula:
 *   UID = PID << N + EXC_ID
 * where:
 *   PID, is the process ID of the RTLib control thread
 *   N, is the value represented by this constant
 *   EXC_ID, the the ID of the specific application EXC
 */
#define BBQUE_UID_SHIFT 5
// This must match the value of BBQUE_UID_SHIFT
#define BBQUE_UID_MASK 0x1F

/* A global variable to signal if we are running as a daemon */
extern unsigned char daemonized;

#endif // BBQUE_CONFIG_H_
